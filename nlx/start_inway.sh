echo "Starting on tcp port: 50040"
docker run --rm \
--name inway_apinovumbrp \
--volume /home/nov_brp/platform/system/public_html/api.gemeente.demo.novum.nu/nlx/root.crt:/certs/root.crt:ro \
--volume /home/nov_brp/platform/system/public_html/api.gemeente.demo.novum.nu/nlx/org.crt:/certs/org.crt:ro \
--volume /home/nov_brp/platform/system/public_html/api.gemeente.demo.novum.nu/nlx/org.key:/certs/org.key:ro \
--volume /home/nov_brp/platform/system/public_html/api.gemeente.demo.novum.nu/nlx/service-config.toml:/service-config.toml:ro \
--env DIRECTORY_REGISTRATION_ADDRESS=directory-registration-api.demo.nlx.io:443 \
--env SELF_ADDRESS=api.gemeente.demo.novum.nu:50030 \
--env SERVICE_CONFIG=/service-config.toml \
--env TLS_NLX_ROOT_CERT=/certs/root.crt \
--env TLS_ORG_CERT=/certs/org.crt \
--env TLS_ORG_KEY=/certs/org.key \
--env DISABLE_LOGDB=1 \
--publish 50030:8443 \
nlxio/inway:latest