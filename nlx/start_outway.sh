echo "Starting on tcp port: 50030"
docker run --rm \
--name outway_apinovumbrp \
--volume /home/nov_brp/platform/system/public_html/api.gemeente.demo.novum.nu/nlx/root.crt:/certs/root.crt:ro \
--volume /home/nov_brp/platform/system/public_html/api.gemeente.demo.novum.nu/nlx/org.crt:/certs/org.crt:ro \
--volume /home/nov_brp/platform/system/public_html/api.gemeente.demo.novum.nu/nlx/org.key:/certs/org.key:ro \
--env DIRECTORY_INSPECTION_ADDRESS=directory-inspection-api.demo.nlx.io:443 \
--env TLS_NLX_ROOT_CERT=/certs/root.crt \
--env TLS_ORG_CERT=/certs/org.crt \
--env TLS_ORG_KEY=/certs/org.key \
--env DISABLE_LOGDB=1 \
--publish 50040:8080 \
nlxio/outway:latest