<?php
namespace ApiNovumBrp;

use Crud\FormManager;
use Crud\IApiAlwaysExpose;
use Crud\IApiEndpointFilter;

class EndpointFilter implements IApiEndpointFilter
{

    function filter(FormManager $oManager):bool
    {

        if($oManager instanceof IApiAlwaysExpose)
        {
            return true;
        }

        if(!method_exists($oManager, 'getTags'))
        {
            return false;
        }
        $aTags = $oManager->getTags();



        if(!in_array('NovumBrp', $aTags))
        {
            return false;
        }


        return true;
    }
}
