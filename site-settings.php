<?php

return [
    'api_version' => 2,
    'config_dir'  => 'novum.brp',
    'namespace'   => 'ApiNovumBrp',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => 'api.gemeente.demo.novum.nu',
    'test_domain' => 'api.test.gemeente.demo.novum.nu',
    'dev_domain'  => 'api.gemeente.innovatieapp.nl',
];
